const express = require('express');
const app = express();
const cors = require('cors');

const port = process.env.PORT || 80;
const base = `${__dirname}/`;

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");
    next();
});
app.use(express.static('./'));


/*
 * ROUTES
*/
app.get('/', (req, res) => {
    res.sendFile(`${base}/public/index.html`);
});


//Web Listener\\
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});