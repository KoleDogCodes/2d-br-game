class HUD {
    constructor(phr) {
      pharser = phr;
    }

    static create () {
        //Creating text for current player location
        txt_loc = pharser.add.text(32, 16, `X: ${player.obj.x}   Y: ${player.obj.y}`, { fontFamily: '"Roboto Condensed"' });
        txt_loc.setScrollFactor(0);

        //Creating player count hud display
        player_count_img = pharser.add.image(pharser.game.config.width - 64, 48, 'player_counter');
        txt_player_count = pharser.add.text(pharser.game.config.width - 90, 30, `  5`, { fontFamily: '"Roboto Condensed"', fontSize: '32px', strokeThickness: 1});
        txt_player_count.setScrollFactor(0);
        player_count_img.setScrollFactor(0);
    }

    static update () {
        txt_loc.setText(`X: ${Math.round(player.obj.x)}   Y: ${Math.round(player.obj.y)}`);
    }
}

var 
txt_loc, 

player_count_img, 
txt_player_count,

pharser;