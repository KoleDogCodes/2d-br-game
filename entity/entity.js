class Enemy {
    constructor(id, pharser, sprite, size) {
        this.obj = sprite;
        this.pharser = pharser;
        this.bound_offset = 250;
        this.world_size = size;
        this.target = null;
        this.id = id;

        this.hands = {
            left: this.pharser.physics.add.sprite(this.obj.x - this.obj.width / 2, this.obj.y - this.hand_distance, 'hand'),
            right: this.pharser.physics.add.sprite(this.obj.y - this.obj.height / 2, this.obj.y + this.hand_distance, 'hand')
        }
    }

    setHandsVelocity(x, y) {
        this.hands.left.setVelocity(x, y);
        this.hands.right.setVelocity(x, y);
    }

    setTarget(pl) {
        this.target = pl;
    }

    distanceBetween(entity1, entity2) {
        const from = new Phaser.Math.Vector2(entity1.x, entity2.y);
        const to = new Phaser.Math.Vector2(entity2.x, entity2.y);

        //return Math.sqrt( Math.pow(to.x - from.x, 2) + Math.pow(to.y - from.y, 2) );
        return from.subtract(to).length();
    }

    update(entites) {
        //Look towards closest entity
        entites.forEach(element => {
            //Check if entity is nearby
            const radius = this.obj.width / 2;
            const from = new Phaser.Math.Vector2(this.obj.x, this.obj.y);
            const to = new Phaser.Math.Vector2(element.obj.x, element.obj.y);

            if (this.distanceBetween(this.obj, element.obj) > 850 || element.id == this.id || element instanceof Player == false) return;
            this.setTarget(element);

            const result = from.subtract(to);

            //Setting position of left hand (First Hand)
            var mouse_angle = result.angle() + (60 * (Math.PI / 180));
            this.hands.left.setPosition(this.obj.x + radius * Math.sin(-mouse_angle), this.obj.y + radius * Math.cos(-mouse_angle));

            //Setting position of right hand (Second Hand)
            mouse_angle = result.angle() + (120 * (Math.PI / 180));
            this.hands.right.setPosition(this.obj.x + radius * Math.sin(-mouse_angle), this.obj.y + radius * Math.cos(-mouse_angle));
        });

        //Move toward cloest enemy
        if (this.target != null && this.target != undefined) {
            const target = this.target;

            const from = new Phaser.Math.Vector2(this.obj.x, this.obj.y);
            const to = new Phaser.Math.Vector2(target.obj.x, target.obj.y);
            const result = from.subtract(to);
            console.log(`Distance: ${this.distanceBetween(this.obj, target.obj)}`);

            //Find new target if target out of range
            if (this.distanceBetween(this.obj, target.obj) > 850) {
                this.obj.setVelocity(0, 0);
                this.target = null;
                console.log('Target lost');
                return true;
            }

            //Enemy velocity = 0 if in range
            if (this.distanceBetween(this.obj, target.obj) <= 15) {
                this.obj.setVelocity(0, 0);
                console.log('In range');
                return true;
            }

            if (result.x > 0) {
                this.obj.setVelocity(-MOVE_SPEED, this.obj.body.velocity.y);
                this.setHandsVelocity(-MOVE_SPEED, this.obj.body.velocity.y);
            }
            else {
                this.obj.setVelocity(MOVE_SPEED, this.obj.body.velocity.y);
                this.setHandsVelocity(MOVE_SPEED, this.obj.body.velocity.y);
            }

            if (result.y > 0) {
                this.obj.setVelocity(this.obj.body.velocity.x, -MOVE_SPEED);
                this.setHandsVelocity(this.obj.body.velocity.x, -MOVE_SPEED);
            }
            else {
                this.obj.setVelocity(this.obj.body.velocity.x, MOVE_SPEED);
                this.setHandsVelocity(this.obj.body.velocity.x, MOVE_SPEED);
            }
        }
    }
}

var MOVE_SPEED = 100;