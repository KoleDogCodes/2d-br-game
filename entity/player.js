var MOVE_SPEED = 500;

class Player {
    constructor(pharser, sprite, size) {
      this.obj = sprite;
      this.pharser = pharser;
      this.bound_offset = 250;
      this.world_size = size;

      this.hands = { 
          left: this.pharser.physics.add.sprite(this.obj.x - this.obj.width / 2, this.obj.y - this.hand_distance, 'hand'),
          right: this.pharser.physics.add.sprite(this.obj.y - this.obj.height / 2, this.obj.y + this.hand_distance, 'hand')
        }
    }

    setHandsVelocity(x, y){
        this.hands.left.setVelocity(x, y);
        this.hands.right.setVelocity(x, y);
    }

    update() {
        //Checks to see if player is border and if so stop the keyboard press velocity.
        if (WKey.isDown){
            if (this.obj.y >= this.bound_offset){
                this.obj.setVelocity(this.obj.body.velocity.x, -MOVE_SPEED);
                this.setHandsVelocity(this.obj.body.velocity.x, this.obj.body.velocity.y);
            }
            else {
                this.obj.setVelocity(this.obj.body.velocity.x, 0);
                this.setHandsVelocity(this.obj.body.velocity.x, 0);
            }
        }
    
        if (AKey.isDown){
            if (this.obj.x >= this.bound_offset){
                this.obj.setVelocity(-MOVE_SPEED, this.obj.body.velocity.y);
                this.setHandsVelocity(-MOVE_SPEED, this.obj.body.velocity.y);
            }
            else {
                this.obj.setVelocity(0, this.obj.body.velocity.y);
                this.setHandsVelocity(0, this.obj.body.velocity.y);
            }
        }
    
        if (SKey.isDown){
            if (this.obj.y <= this.world_size - this.bound_offset){
                this.obj.setVelocity(this.obj.body.velocity.x, MOVE_SPEED);
                this.setHandsVelocity(this.obj.body.velocity.x, this.obj.body.velocity.y);
            }
            else {
                this.obj.setVelocity(this.obj.body.velocity.x, 0);
                this.setHandsVelocity(this.obj.body.velocity.x, 0);
            }
        }
    
        if (DKey.isDown){
            if (this.obj.x <= this.world_size - this.bound_offset){
                this.obj.setVelocity(MOVE_SPEED, this.obj.body.velocity.y);
                this.setHandsVelocity(MOVE_SPEED, this.obj.body.velocity.y);
            }
            else {
                this.obj.setVelocity(0, this.obj.body.velocity.y);
                this.setHandsVelocity(0, this.obj.body.velocity.y);
            }
        }
    }

    mouse_listener(){
        this.pharser.input.on('pointermove', (pointer, gameobj) => {
            const radius = this.obj.width / 2;
            const from = new Phaser.Math.Vector2(this.obj.x, this.obj.y);
            const to = new Phaser.Math.Vector2(pointer.worldX, pointer.worldY);
            const result = from.subtract(to);

            //Setting position of left hand (First Hand)
            var mouse_angle = result.angle() + (60 * (Math.PI / 180));
            this.hands.left.setPosition(this.obj.x + radius * Math.sin(-mouse_angle), this.obj.y + radius * Math.cos(-mouse_angle));

            //Setting position of right hand (Second Hand)
            mouse_angle = result.angle() + (120 * (Math.PI / 180));
            this.hands.right.setPosition(this.obj.x + radius * Math.sin(-mouse_angle), this.obj.y + radius * Math.cos(-mouse_angle));
        });
    }

    keyboard_listener() {
        //KEYUP EVENTS
        this.pharser.input.keyboard.on('keyup-W', () =>{
            this.obj.setVelocity(this.obj.body.velocity.x, 0);
            this.setHandsVelocity(this.obj.body.velocity.x, 0);
        });
    
        this.pharser.input.keyboard.on('keyup-A', () =>{
            this.obj.setVelocity(0, this.obj.body.velocity.y);
            this.setHandsVelocity(0, this.obj.body.velocity.y);
        });
    
        this.pharser.input.keyboard.on('keyup-S', () =>{
            this.obj.setVelocity(this.obj.body.velocity.x, 0);
            this.setHandsVelocity(this.obj.body.velocity.x, 0);
        });
    
        this.pharser.input.keyboard.on('keyup-D', () =>{
            this.obj.setVelocity(0, this.obj.body.velocity.y);
            this.setHandsVelocity(0, this.obj.body.velocity.y);
        });
    }
}